<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GifResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'file' => asset($this->file),
            'created_at' => $this->created_at,
            'user_id' => $this->user->id,
            'user_name' => $this->user->name
        ];
    }
}
