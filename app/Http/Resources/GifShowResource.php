<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GifShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = (new GifResource($this))->toArray(null);
        $data['tags'] = $this->tags->pluck('name');
        return $data;
    }
}
