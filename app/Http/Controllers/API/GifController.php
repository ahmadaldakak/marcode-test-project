<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Gif\StoreReuest;
use App\Http\Requests\Gif\UpdateRequest;
use App\Http\Resources\GifResource;
use App\Http\Resources\GifShowResource;
use App\Models\Gif;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class GifController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->only(['store', 'update', 'delete']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tags = Tag::where('name', 'like', "%$request->search%")->get()->pluck('id')->toArray();
        $images = Gif::with('user')->whereHas('tags', function ($q) use ($tags) {
            $q->whereIn('tag_id', $tags);
        })->simplePaginate(20);
        $images->appends($request->all());
        return $this->sendResponse(GifResource::collection($images)->response()->getData());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreReuest $request)
    {
        $data = $request->all();
        $data['tags'] = explode(',', $data['tags']);
        foreach ($data['tags'] as &$tag) {
            $tag = Tag::firstOrCreate(['name' => $tag])->id;
        }
        $data['file'] = 'storage/' . $data['file']->store(
            'images',
            'public'
        );
        $gif = Gif::create($data);
        $gif->tags()->sync($data['tags']);
        return $this->sendResponse(new GifShowResource($gif),'gif added sccessfuly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Gif $gif)
    {
        $image = new GifShowResource($gif);
        return $this->sendResponse($image);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Gif $gif)
    {
        $this->authorize('update',$gif);
        $data = $request->only(['tags']);
        $data['tags'] = explode(',', $data['tags']);
        foreach ($data['tags'] as &$tag) {
            $tag = Tag::firstOrCreate(['name' => $tag])->id;
        }
        $gif->tags()->sync($data['tags']);
        return $this->sendMessage('update successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gif $gif)
    {
        File::delete($gif->file);
        $gif->delete();
        return $this->sendMessage('gif delete sccessfuly');
    }
}
