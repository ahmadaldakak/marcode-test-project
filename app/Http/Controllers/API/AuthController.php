<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\SignUpRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        $user = User::ByCredential($request->credential)->first();
        if ($user && Hash::check($request->password,$user->password)) {
            return $this->sendResponse([
                'user' => $user,
                'token' => $user->createToken(env('APP_NAME'))->accessToken
            ], __('messages.login_successfuly', ['attr' => env('APP_NAME')]));
        } else {
            return $this->sendMessage(__('auth.password'));
        }
        return $this->sendMessage(__('auth.failed'));
    }

    public function log_out()
    {
        auth()->user()->token()->delete();
        return $this->sendMessage(__('messages.ok'));
    }

    public function sign_up(SignUpRequest $request)
    {
        $user = User::create($request->all());
        return $this->sendResponse([
            'access_token' => $user->createToken(env('APP_NAME'))->accessToken
        ], __('messages.ok'));
    }
    
    public function profile()
    {
        return $this->sendResponse(auth()->user());
    }
}
