<?php

namespace App\Http\Requests\Gif;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\Response;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tags' => ['required','string']
        ];
    }
}
