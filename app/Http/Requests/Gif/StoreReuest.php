<?php

namespace App\Http\Requests\Gif;

use Illuminate\Foundation\Http\FormRequest;

class StoreReuest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => ['required','file','mimes:gif'],
            'tags' => ['required','string']
        ];
    }

    protected function withValidator()
    {
        $this->merge([
            'user_id' => auth()->user()->id
        ]);
    }
}
