<?php

namespace App\Policies;

use App\Models\Gif;
use App\Models\User;
use App\Traits\Response as TraitsResponse;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class GifPolicy
{
    use HandlesAuthorization, TraitsResponse;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Gif  $gif
     * @return mixed
     */
    public function view(User $user, Gif $gif)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Gif  $gif
     * @return mixed
     */
    public function update(User $user, Gif $gif)
    {
        return $user->id == $gif->user_id
            ? Response::allow()
            : Response::deny('You do not own this gif.');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Gif  $gif
     * @return mixed
     */
    public function delete(User $user, Gif $gif)
    {
        return $user->id == $gif->user_id
            ? Response::allow()
            : Response::deny('You do not own this gif.');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Gif  $gif
     * @return mixed
     */
    public function restore(User $user, Gif $gif)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Gif  $gif
     * @return mixed
     */
    public function forceDelete(User $user, Gif $gif)
    {
        //
    }
}
