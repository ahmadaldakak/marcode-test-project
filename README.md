# Marcode Test Project (php Laravel)

## Introduction

An introduction or lead on what problem you're solving. Answer the question, "Why does someone need this?"

## Installation

### Create your .env File
> you can copy and paste .env.example file ;)

### Install composer packages
```bash
$ composer install
```


### Generate Application Key
```bash
$ php artisan key:generate
```

### Link storage
```bash
$ php artisan storage:link
```

### Give storage folder permission
```bash
$ chmod -R 777 storage/
```

### Migrate Database
```bash
$ php artisan migrate
```

### Install passport keys
```bash
$ php artisan passport:install
```
> This command will create the encryption keys needed to generate secure access tokens. In addition, the command will create "personal access" and "password grant" clients which will be used to generate access tokens.

### Seeding Database
```bash
$ php artisan db:seed
```

> You can get postman collication from src folder (marcode test project.postman_collection.json)