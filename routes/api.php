<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\GifController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('auth/login', [AuthController::class, 'login']);
Route::post('auth/sign_up', [AuthController::class, 'sign_up']);
Route::middleware(['auth:api'])->group(function () {
    Route::post('auth/log_out', [AuthController::class, 'log_out']);
    Route::get('auth/profile', [AuthController::class, 'profile']);
});
Route::apiResource('gifs', GifController::class);
