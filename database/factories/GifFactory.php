<?php

namespace Database\Factories;

use App\Models\Gif;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class GifFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Gif::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::get()->random(1)->first()->id,
            'file' =>"storage/images/".$this->faker->image('public/storage/images',30,30,'animals',false)
        ];
    }
    public function configure()
    {
        return $this->afterMaking(function (Gif $gif) {
            //
        })->afterCreating(function (Gif $gif) {
            $gif->tags()->sync(Tag::get()->random(5));
        });
    }
}
